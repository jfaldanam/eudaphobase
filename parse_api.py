import argparse
import datetime
from functools import cache
from pathlib import Path

import requests
from jinja2 import Template
from rdflib import Graph, Literal, Namespace, RDF, URIRef
from rdflib.namespace import RDF, XSD, RDFS, VANN

# Define global namespaces
tax = Namespace('https://w3id.org/EUTaxO#')
darwin = Namespace('http://rs.tdwg.org/dwc/terms/')
owl = Namespace('http://www.w3.org/2002/07/owl#')
terms = Namespace('http://purl.org/dc/terms/')
cc = Namespace('http://creativecommons.org/ns#')

def create_graph(ontology_uri: Namespace, kingdom: str) -> Graph:
    """
    Create a new graph importing EUTaxO and bind namespaces to prefix.
    """
    # Load EUTaxO's template
    # Create a Template object
    with open("EUTaxO-template.owl.jinja2", "r") as f:
        template = Template(f.read())

    prefix = f"tax{kingdom[0].lower()}"

    # Generate new graph
    graph = Graph()

    # Bind namespaces to prefix TODO: not rendering in the final ontology
    graph.bind("", Namespace(ontology_uri))
    graph.bind(prefix, Namespace(ontology_uri + "#"))

    # Render the template with actual values
    rendered_template = template.render(base_url=str(ontology_uri), prefix=prefix, kingdom=kingdom, date_=datetime.datetime.now().isoformat())

    # Load the rendered template into the graph
    graph.parse(data=rendered_template, format="xml")

    return graph

def get_rank(id):
    ranks = [rank["displayName"]["en"] for rank in taxon_ranks() if rank["id"]==id]
    return ranks[0]

def get_position(id):
    ranks = [int(rank["attributes"]["position"]) for rank in taxon_ranks() if rank["id"]==id]
    return ranks[0]

def build_partial_name(taxon_object, individual: bool = False):
    id_ = str(taxon_object["id"]).replace(" ", "_").replace("\u00a0", "")
    name = taxon_object['name'].replace(" ", "_").replace("\u00a0", "")
    if individual:
        return f"Individual_{id_}_{name}"
    return f"{id_}_{name}"

def parse_top_level_objects(database: dict, format_: str, output_folder: Path = None) -> None:
    """
    Parse top level objects (kingdoms) and their children.
    """
    if output_folder is None:
        output_folder = Path.cwd()

    for kingdom_object in database:
        kingdom_name = kingdom_object['name'].lower()
        base_uri_kingdom = f"https://w3id.org/EUTaxO/{kingdom_name}"
        kingdom_namespace = Namespace(base_uri_kingdom + "#")
        kingdom_graph = create_graph(base_uri_kingdom, kingdom_name)
        parse_children(kingdom_graph, kingdom_namespace, kingdom_object, parent_node=None, parent_class=None, genus=None, species=None, kingdom=None, phylum=None)
        # Store RDF data
        kingdom_folder = output_folder / kingdom_name
        kingdom_folder.mkdir(parents=True, exist_ok=True)
        with open(kingdom_folder / f"EUTaxO-{kingdom_name}.owl", 'w+') as f:
            f.write(kingdom_graph.serialize(format=format_))

def parse_children(graph: Graph, ns: Namespace, children_object, parent_node, parent_class, genus, species, kingdom, phylum):
    partial_name = build_partial_name(children_object)
    class_node = URIRef(ns + partial_name)
    if "rank" in children_object and get_rank(children_object["rank"]) == 'Kingdom':
        individual_partial_name = build_partial_name(children_object, individual=True)
        kingdom = URIRef(ns + individual_partial_name)
    if "rank" in children_object and get_rank(children_object["rank"]) == 'Phylum':
        individual_partial_name = build_partial_name(children_object, individual=True)
        phylum = URIRef(ns + individual_partial_name)
    node = add_metadata(graph, ns, class_node, children_object, parent_node=parent_node, parent_class=parent_class, genus=genus, species=species, kingdom=kingdom, phylum=phylum)
    
    parent_class = class_node
    parent_node = node
    parent_object = children_object
    if 'children' in parent_object:
        if "rank" in parent_object and get_rank(parent_object["rank"]) == 'Genus':
            #print(f'Genus {parent_object["name"]}')
            genus = parent_object["name"]
        
        if "rank" in parent_object and get_rank(parent_object["rank"]) == 'Species':
            #print(f'Species {parent_object["name"]}')
            species = parent_object["name"]
        
        for children in parent_object['children']:
            parse_children(graph, ns, children, parent_node=parent_node, parent_class=parent_class, genus=genus, species=species, kingdom=kingdom, phylum=phylum)

def add_metadata(graph: Graph, ns: Namespace, class_node, object, parent_node=None, parent_class=None, genus=None, species=None, kingdom=None, phylum=None):
    # Create class hierarchy
    if parent_node is None:
        graph.add( (class_node, RDFS.subClassOf, tax.Taxon) )
    else:
        graph.add( (class_node, RDFS.subClassOf, parent_class) )
    
    graph.add( (class_node, RDF.type, owl.Class) )
    graph.add( (class_node, RDFS.label, Literal(object["name"], datatype = XSD.string)) )

    uri = build_partial_name(object, individual = True)
    node = URIRef(ns + uri)
    
    # Add RDF to graph
    graph.add( (node, RDF.type, class_node) )

    # Name
    uses_brackets = bool(object["brackets"]) if 'brackets' in object else False
    if "author" not in object:
        if "year" not in object: # There is no year nor author
            authorship = ""
        else: # There is no author
            authorship = object["year"]
    elif "year" not in object: # There is no year
            authorship = object["author"]
    else: # There is both
        authorship = f"{object['author']}, {object['year']}"
    
    if uses_brackets:
        authorship = f"({authorship})"

    if genus:
        if "rank" in object and get_rank(object["rank"]) == 'Group':
            name = ('%s (%s) %s %s' % (genus, species, object["name"], authorship))
        elif "rank" in object and get_rank(object["rank"]) == 'Subspecies':
            name = ('%s %s %s %s' % (genus, species, object["name"], authorship))
        else:
            name = ('%s %s %s' % (genus, object["name"], authorship))
    else:
        name = ('%s %s' % (object["name"], authorship))

    # Remove extra spaces in case of missing authorship
    name = name.strip().replace("\u00a0", "")
    graph.add( (node, darwin.scientificName, Literal(name)) )
    graph.add( (node, RDFS.label, Literal(name)) )

    # ID
    graph.add( (node, tax.id, Literal(object["id"])) )

    # Author
    if "author" in object:
        graph.add( (node, darwin.scientificNameAuthorship, Literal(object["author"].strip().replace("\u00a0", ""))) )

    # Year
    if "year" in object:
        graph.add( (node, darwin.namePublishedInYear, Literal(object["year"])) )

    # Brackets
    if 'brackets' in object:
        brackets = object["brackets"]
        graph.add( (node, tax.brackets, Literal(brackets, datatype=XSD.boolean)) )

    # Additional nomenclature
    # nomenclature = metadata.get('additional nomenclature')
    # if not isnan(nomenclature):
    #     graph.add( (node, tax.XXXX, Literal(nomenclature)) )

    # Valid species
    # valid_species = metadata.get('valid species')
    # if not isnan(valid_species):
    #     valid_species = str_to_bool(valid_species)
    #     graph.add( (node, tax.XXXX, Literal(valid_species, datatype=XSD.boolean)) )

    # Synonyms
    if 'synonyms' in object:
        for synonym in object['synonyms']:
            uri_synonym = URIRef(tax + build_partial_name(synonym, individual = True))
            #TODO: change URI
            graph.add( (node, tax.synonym, uri_synonym) )
            graph.add( (uri_synonym, tax.synonym, node) )

    # Synonyms exists
    synonyms_exist = 'synonyms' in object
    graph.add( (node, tax.synonyms_exist, Literal(synonyms_exist, datatype=XSD.boolean)) )

    # Genus
    if genus:
        graph.add( (node, darwin.genus, Literal(genus)) )

    # Specific epithet
    if "rank" in object and get_position(object["rank"]) >= get_position(11632):# Its lower on the hierarchy than species
        graph.add( (node, darwin.specificEpithet, Literal(object["name"])) )

    # Taxon rank
    if "rank" in object:
        taxon_rank = object["rank"]
        graph.add( (node, darwin.taxonRank, Literal(get_rank(taxon_rank))) )
    
    # Accepted Name Usage
    if "valid" in object:
        if object["valid"]:
            acceptedNameUsage = name
        else:
            if 'synonyms' in object:
                for synonym in object['synonyms']:
                    if "valid" in synonym and synonym["valid"]:
                        acceptedNameUsage = synonym["displayName"]

    graph.add( (node, darwin.acceptedNameUsage, Literal(acceptedNameUsage)) )

    # Add phylum and kingdom
    if kingdom:
        graph.add( (node, tax.kingdom, kingdom) )
    if phylum:
        graph.add( (node, tax.phylum, phylum) )

    # Belongs to
    if parent_node:
        graph.add( (node, tax.belongsTo, parent_node) )

    return node

@cache
def taxon_database() -> dict:
    """Get the taxon database from the API"""
    response = requests.get('https://api.edaphobase.org/taxon/getRootTaxa?depth=-1&childFormat=array')
    response.raise_for_status()

    return response.json()

@cache
def taxon_ranks():
    """
    Get the taxon ranks from the API. Id 45 is the taxon rank list.
    """
    response = requests.get('https://api.edaphobase.org/list/elementsByList/45')
    response.raise_for_status()

    return response.json()

######## MAIN ############
if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '-o', '--output', default=".",
        help="Output folder")
    parser.add_argument(
        '-f', '--format', default="xml",
        help="RDF serialization format supported by RDFlib: https://rdflib.readthedocs.io/en/stable/plugin_serializers.html")

    args = parser.parse_args()
    output_folder = Path(args.output).resolve()
    format_ = args.format

    # Parse the EDaphobase database
    parse_top_level_objects(taxon_database(), format_, output_folder=output_folder)